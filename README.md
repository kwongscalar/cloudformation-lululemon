# Cloudformation Lululemon #

This is the repo contains the Cloudformation (CF) templates developed for Lululemon to
create their VPC, Oracle Commerce application stack and Oracle RDS DB.

### Requirements ###

* Access to Lululemon and/or Scalar AWS account

### How does it work ###

Upload the CF templates in the following order using the AWS console:

```
lululemon-cf-vpc-v#.json
lululemon-cf-app-v#.json
lululemon-cf-rds-v#.json
```

### TODO ###
