{
  "AWSTemplateFormatVersion": "2010-09-09",
  
  "Description": "This template creates the MultiAZ Oracle RDS instances",

  "Parameters": {
    "DBName" : {
      "Description" : "The name to be used for the RDS Database",
      "Type": "String",
      "MinLength": "1",
      "Default": "atgdvn01"
    },
    "MasterUsername": {
        "Description": "User name to manage Database instance",
        "Type": "String",
        "Default": "awssys",
        "MinLength": "5",
        "MaxLength": "25",
        "AllowedPattern": "[a-zA-Z0-9]*"
    },
    "MasterUserPassword": {
        "Description": "Password for the master user. Must be at least 8 characters containing letters, numbers and symbols",
        "Type": "String",
        "MinLength": "8",
        "MaxLength": "32",
        "AllowedPattern": "(?=^.{6,255}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*",
        "NoEcho": "true"
    },
    "VPCID": {
        "Description": "ID of the VPC that you want to provision EFS into",
        "Type": "AWS::EC2::VPC::Id"
    },
    "Subnet1": {
        "Description": "ID of the public subnet 1 that you want to provision the first Remote Desktop Gateway into (e.g., subnet-a0246dcd)",
        "Type": "AWS::EC2::Subnet::Id"
    },
    "Subnet2": {
        "Description": "ID of the public subnet 2 you want to provision the second Remote Desktop Gateway into (e.g., subnet-e3246d8e)",
        "Type": "AWS::EC2::Subnet::Id"
    },
    "VPCCIDR": {
        "AllowedPattern": "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/([0-9]|[1-2][0-9]|3[0-2]))$",
        "Description": "The VPC CIDR block used for SG access to RDS",
        "Type": "String",
        "Default": "10.201.28.0/24"
    },
    "CorpCIDR" : {
      "Description" : "Please enter CIDR of the corporate network (xxx.xxx.xxx.xxx/xx):",
        "Type": "String",
        "MinLength": "1",
        "Default": "10.128.0.0/10"
    }
  },
  "Resources": {
    "lulurds": {
      "Type": "AWS::RDS::DBInstance",
      "Properties": {
        "AllocatedStorage": "100",
        "AllowMajorVersionUpgrade": "false",
        "AutoMinorVersionUpgrade": "true",
        "CharacterSetName": "AL32UTF8",
        "DBInstanceClass": "db.m4.xlarge",
        "Port": "1521",
        "StorageType": "io1",
        "BackupRetentionPeriod": "7",
        "MasterUsername": {"Ref":"MasterUsername"},
        "MasterUserPassword": {"Ref":"MasterUserPassword"},
        "PreferredBackupWindow": "00:00-00:30",
        "PreferredMaintenanceWindow": "sun:01:00-sun:01:30",
        "Iops": "1000",
        "DBName": {"Ref":"DBName"},
        "Engine": "oracle-ee",
        "EngineVersion": "11.2.0.4.v10",
        "LicenseModel": "bring-your-own-license",
        "MultiAZ": "true",
        "DBSubnetGroupName": {
          "Ref": "dbsubnet"
        },
        "VPCSecurityGroups": [
          {
            "Ref": "sgRDS"
          }
        ],
        "Tags": [
          {
            "Key": "workload-type",
            "Value": "production"
          }
        ]
      }
    },
    "dbsubnet": {
      "Type": "AWS::RDS::DBSubnetGroup",
      "Properties": {
        "DBSubnetGroupDescription": "Created by CloudFormation",
        "SubnetIds": [
          {"Ref":"Subnet1"},
          {"Ref":"Subnet2"}
        ]
      }
    },
    "sgRDS": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Created from the RDS Management Console",
        "VpcId": {"Ref":"VPCID"}
      }
    },
    "ingress3": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "sgRDS"
        },
        "IpProtocol": "tcp",
        "FromPort": "1521",
        "ToPort": "1521",
        "CidrIp": {"Ref": "VPCCIDR" }
      }
    },
    "ingress4": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "sgRDS"
        },
        "IpProtocol": "tcp",
        "FromPort": "1521",
        "ToPort": "1521",
        "CidrIp": {"Ref": "CorpCIDR" }
      }
    },    
    "egress3": {
      "Type": "AWS::EC2::SecurityGroupEgress",
      "Properties": {
        "GroupId": {
          "Ref": "sgRDS"
        },
        "IpProtocol": "-1",
        "CidrIp": "0.0.0.0/0"
      }
    }
  },
  "Outputs" : {
    "RDSEndPointID" : {
      "Description" : "RDS EndPoint ID",
      "Value" :  {"Fn::GetAtt" : ["lulurds","Endpoint.Address"]},
      "Export" : { "Name" : {"Fn::Sub": "${AWS::StackName}-RDSEndpoint"} }}
  }
}
