{
  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "Create an EC2 instance running RHEL 6.8 with a new EBS volume attached. ",

  "Parameters" : {
    "KeyName" : {
      "Description" : "Name of an existing EC2 Keypair to enable SSH access into the server",
      "Type" : "AWS::EC2::KeyPair::KeyName",
      "MinLength": "1",
      "MaxLength": "64",
      "AllowedPattern" : "[-_ a-zA-Z0-9]*",
      "ConstraintDescription" : "can contain only alphanumeric characters, spaces, dashes and underscores."
    },
    "InstanceType" : {
      "Description" : "The type of EC2 instance to launch",
      "Type" : "String",
      "Default" : "t2.small",
      "AllowedValues" : [ "t2.micro","t2.small","t2.medium","t2.large","m4.large","m3.medium","m3.large"],
      "ConstraintDescription" : "must be a valid and allowed EC2 instance type."
    },
    "VolumeSize" : {
      "Description" : "The size of the EBS volume to attach",
      "Type" : "Number",
      "Default" : "150"
    },
    "SecurityGroup" : {
      "Description" : "The security group to add the EC2 instance to",
      "Type" : "String",
      "Default" : "LululemonVPCSecurityGroup"
    },
    "VPCID": {
      "Description": "ID of the VPC where you want to provision the servers.",
      "Type": "AWS::EC2::VPC::Id"
    },
    "WebSubnet1": {
        "Description": "ID of first private web subnet (e.g., subnet-a0246dcd)",
        "Type": "AWS::EC2::Subnet::Id"
    },
    "WebSubnet2": {
        "Description": "ID of second private web subnet (e.g., subnet-a0246dcd)",
        "Type": "AWS::EC2::Subnet::Id"
    },
    "AppSubnet1": {
        "Description": "ID of first private app subnet (e.g., subnet-a0246dcd)",
        "Type": "AWS::EC2::Subnet::Id"
    },
    "AppSubnet2": {
        "Description": "ID of second private app subnet (e.g., subnet-a0246dcd)",
        "Type": "AWS::EC2::Subnet::Id"
    },
  },
  "Mappings" : {
    "AWSInstanceType2Arch" : {
      "m4.large" : {"Arch" : "64" },
      "t2.micro" : {"Arch" : "64" },
      "t2.small" : {"Arch" : "64" },
      "t2.medium" : {"Arch" : "64" },
      "t2.large" : {"Arch" : "64" },
      "m3.medium" : {"Arch" : "64" },
      "m3.large" : {"Arch" : "64" },
      "t2.small" : {"Arch" : "64" }
    },
    "AWSRegionArch2AMI" : {
      "us-east-1" : {"64" : "ami-1a0a540d"},
      "us-west-1" : {"64" : "ami-6fb7450f"}
    }
  },
  "Resources" : {
    "Ec2InstanceWeb1" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "ImageId" :   { "Fn::FindInMap" : [ "AWSRegionArch2AMI", { "Ref" : "AWS::Region" }, 
                      { "Fn::FindInMap" : [ "AWSInstanceType2Arch", {"Ref" : "InstanceType"}, "Arch"] } ] },
        "InstanceType" : {"Ref" : "InstanceType" },
        "SecurityGroupIds" : [{ "Ref": "LululemonVPCSecurityGroup" }],
        "AvailabilityZone" : "us-east-1a",
        "SubnetId" : {"Ref" : "WebSubnet1" },
        "KeyName" : {"Ref" : "KeyName" },
        "Tags" : [
          { 
            "Key" : "Name",
            "Value" : {"Ref" : "AWS::StackName"} 
          }
        ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : ["", [
           "#!/bin/bash -v\n",
           "## Wait for EBS mount to become available before layering on file system\n",
           "while [ ! -e /dev/xvdb ]; do echo waiting for /dev/xvdb to attach; sleep 10; done\n",
           "file -s /dev/xvdb\n",
           "mkfs -t ext4 /dev/xvdb\n",
           "mkdir /opt\n",
           "mount /dev/xvdb /opt\n",
           "echo \"/dev/xvdb /opt ext4 defaults,nofail 0 2\" >> /etc/fstab\n"
        ]]}}
        }
      },
    "VolumeWeb1" : {
      "Type" : "AWS::EC2::Volume",
      "Properties" : {
        "Size" : "10",
        "AvailabilityZone" : { "Fn::GetAtt" : [ "Ec2InstanceWeb1", "AvailabilityZone" ]},
        "Tags" : [
          { 
            "Key" : "Name",
            "Value" : {"Ref" : "AWS::StackName"} 
          }
        ]
      }
    },
    "MountPointWeb1" : {
      "Type" : "AWS::EC2::VolumeAttachment",
      "Properties" : {
        "InstanceId" : {"Ref" : "Ec2InstanceWeb1"},
        "VolumeId" : {"Ref" : "VolumeWeb1"},
        "Device" : "/dev/sdb"
      }
    },
    "Ec2InstanceWeb2" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "ImageId" :   { "Fn::FindInMap" : [ "AWSRegionArch2AMI", { "Ref" : "AWS::Region" }, 
                      { "Fn::FindInMap" : [ "AWSInstanceType2Arch", {"Ref" : "InstanceType"}, "Arch"] } ] },
        "InstanceType" : {"Ref" : "InstanceType" },
        "SecurityGroupIds" : [{ "Ref": "LululemonVPCSecurityGroup" }],
        "AvailabilityZone" : "us-east-1c",
        "SubnetId" : {"Ref" : "WebSubnet2" },
        "KeyName" : {"Ref" : "KeyName" },
        "Tags" : [
          { 
            "Key" : "Name",
            "Value" : {"Ref" : "AWS::StackName"} 
          }
        ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : ["", [
           "#!/bin/bash -v\n",
           "## Wait for EBS mount to become available before layering on file system\n",
           "while [ ! -e /dev/xvdb ]; do echo waiting for /dev/xvdb to attach; sleep 10; done\n",
           "file -s /dev/xvdb\n",
           "mkfs -t ext4 /dev/xvdb\n",
           "mkdir /opt\n",
           "mount /dev/xvdb /opt\n",
           "echo \"/dev/xvdb /opt ext4 defaults,nofail 0 2\" >> /etc/fstab\n"
        ]]}}
        }
      },
    "VolumeWeb2" : {
      "Type" : "AWS::EC2::Volume",
      "Properties" : {
        "Size" : "10",
        "AvailabilityZone" : { "Fn::GetAtt" : [ "Ec2InstanceWeb2", "AvailabilityZone" ]},
        "Tags" : [
          { 
            "Key" : "Name",
            "Value" : {"Ref" : "AWS::StackName"} 
          }
        ]
      }
    },
    "MountPointWeb2" : {
      "Type" : "AWS::EC2::VolumeAttachment",
      "Properties" : {
        "InstanceId" : {"Ref" : "Ec2InstanceWeb2"},
        "VolumeId" : {"Ref" : "VolumeWeb2"},
        "Device" : "/dev/sdb"
      }
    },

    "LululemonVPCSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Enable SSH access to VpcId",
        "VpcId" : { "Ref" : "VPCID" },
        "SecurityGroupIngress" : [ { "IpProtocol" : "tcp", "FromPort" : "22", "ToPort" : "22", "CidrIp" : "0.0.0.0/0" } ],
      }
    }
  },

  "Outputs" : {
    "ServerDNS" : {
      "Description" : "DNS name of launched server",
      "Value" : { "Fn::GetAtt" : [ "Ec2InstanceWeb1", "PublicDnsName" ] }
    }
  }
}
